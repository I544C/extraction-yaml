#cadrage:
#questions: il faut bien recuperer les tasks et les label ? oui
#dans le cas d'une task sans label ? mettre la balise description mais remplir avec blanc
#faut-il recuperer les task-type ou autres informations sur les tasks ? non
#quelle forme cela doit il prendre : par exemple task/label ? balise qui dit task espace nom de task puis ligne du dessous description espace : espace label, 
#sous quel format les enregistrer ? fichier texte ? oui
#potentiellement partie de code qui retranscrit en liste de listes ? [[nom, description][nom, description]] non

#plan:
#objectif : import module de lecture yaml
# ! la commande pip install se fait dans le cmd
#parser le texte 
#definir des correspondances -> utilisation de dictionnaire ?
#ecrire dans le fichier texte
#faire attention a la mise en page
#prendre en compte la possibilite de label inexistant



 #---------- code ----------



#importation module permettant import du fichier yaml
import yaml
import os
from os import listdir

#definition noms fichiers utiles
yamlpath = str(input('folder path for yaml files to find ? : '))
txtpath = str(input('folder path for text documents ? : '))
yamliste_s = os.listdir(yamlpath)
yamliste_e = []
for i in range (len(yamliste_s)):
    j = len(yamliste_s[i])
    if yamliste_s[i][j-5:j] == '.yaml':
        yamliste_e.append(yamliste_s[i])

for k in range (len(yamliste_e)):
    yamlfile = yamliste_e[k]

    yamlname = yamlpath + '\\' + yamlfile 
    filename = txtpath  + '\\' + yamlfile + '_doc.txt'
    
    #création/ réinitialisation fichier texte 
    f = open(filename, 'x')
    with open(filename) as f:    
        f = open(filename, 'w')

    #extraction contenu fichier yaml sous forme de chaine de caracteres
    with open(yamlname, "r") as test:
        texte = str(yaml.safe_load(test))

    #modification fichier texte pour permettre mise a la ligne 'label' via -> '}'
    schain = 'label'
    count1 = texte.count(schain)
    index = 0
    for i in range (count1):
        next = texte[index:].find(schain)
        index=index + next
        texte = texte[:index] + '}' +texte[index:]
        while texte[index] != ",":
            index = index + 1
        index = index + 1
        texte = texte[:index] + '}' +texte[index:]

    #changement nomonclature texte yaml pour eviter bugs
    liste = list(texte)
    for i in range (len(liste)):
        if liste[i] == " ":
            liste[i] = "&"
        elif liste[i] == "{" or liste[i] == "}":
            liste[i] = " "
        elif liste[i] == "'":
            liste[i] = "&"
        elif liste[i] == "\"":
            liste[i] = "\""

    #creation liste utilisable pour remplir le .txt
    texte = str("".join(liste))
    liste2 = texte.split()

    #remise de la nomonclature afin de rendre le fichier plus lisible
    for i in range (len(liste2)):
        tempoliste = list(liste2[i])
        for j in range (len(tempoliste)):
            if tempoliste[j] == "&":
                tempoliste[j] = " "
        liste2[i] = str("".join(tempoliste))
        liste2[i] = liste2[i].strip(", :'\"")

    #remplissage du fichier.txt avec les lignes selectionnées
    with open(filename) as f:
        f = open(filename, 'a')
        for i in range (len(liste2)):
            if liste2[i][0:5] == "task_" and liste2[i][0:9] != "task_type":
                f.write("Nom de la tâche : ")
                f.write(liste2[i])
                f.write("\n")
                for j in range (i + 1, len(liste2 )):
                    if liste2[j][0:5] == "label":
                        tempoliste = list(liste2[j])
                        for i in range (5):
                            tempoliste.pop(0)
                        tempoliste.insert(0, 'Description')
                        liste2[j] = str("".join(tempoliste))
                        f.write(liste2[j])
                        f.write("\n")
                        f.write("\n")
                        i = j
                        break
                    elif liste2[j][0:5] == "task_" and liste2[j][0:9] != "task_type":
                        f.write("Description : ")
                        f.write("\n")
                        f.write("\n")
                        i = j - 1
                        break
                    else:
                        j = j + 1